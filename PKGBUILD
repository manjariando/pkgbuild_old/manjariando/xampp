# Maintainer: grufo <madmurphy333 AT gmail DOT com>
# Contributor: Marcin (CTRL) Wieczorek <marcin@marcin.co>
# Contributor: Julio González <juliolokoo at gmail dot com>
# Contributor: Jose Valecillos <valecillosjg (at) gmail (dot) com>
# Contributor: Thiago Perrotta <echo dGhpYWdvcGVycm90dGE5NUBnbWFpbC5jb20K | base64 -d >
# Contributor: alegotta <algottardi at disroot dot org>
# Contributor: oguzkagan <me@oguzkaganeren.com.tr>

pkgname=xampp
_srcver=8.0.13
_binver=0
pkgrel=1
# This PKGBUILD deals with two different versioning formats: the upstream
# format MAJOR.MINOR.REVISION-BUILD - used internally by XAMPP - and the
# makepkg-friendly format MAJOR.MINOR.REVISION[.BUILD]-RELEASE (where the only
# hyphen is the one automatically added by makepkg before `$pkgrel`).
pkgver="${_srcver}$(test "${_binver}" -eq 0 || echo ".${_binver}")"
pkgdesc='A stand-alone LAMPP distribution'
url='https://www.apachefriends.org'
license=('GPL')

# This PKGBUILD is configured for both 32-bit and 64-bit architectures, but
# currently no binaries for 32-bit architectures are distributed. The last
# versions where these were available are 5.6.23-0 and 7.0.8-0. If you want
# to include a 32-bit release add 'i686' to the array below and provide a
# source file.
arch=('x86_64')

makedepends=('sdx' 'tclkit' 'rsync' 'pcre' 'imagemagick')
source=("https://metainfo.manjariando.com.br/${pkgname}/org.xampp-control-panel.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/org.xampp-manager.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/xampp.png"
        'org.apachefriends.xampp.policy'
        'xampp-control-panel.desktop'
        'xampp-control-panel.in'
        'xampp-manager.desktop'
        'bitrock-unpacker.tcl'
        'xampp.tmpfiles.in'
        'properties.ini.in'
        'xampp.service.in'
        'xampp-manager.in'
        'xampp.sysusers'
        'xampp.svg')
_build32name='linux'
_build64name='linux-x64'
source_i686=("${url}/${pkgname}-files/${_srcver}/${pkgname}-${_build32name}-${_srcver}-${_binver}-installer.run")
source_x86_64=("${url}/${pkgname}-files/${_srcver}/${pkgname}-${_build64name}-${_srcver}-${_binver}-installer.run")
options=('staticlibs' 'libtool' '!strip')
install='xampp.install'
sha256sums=('63eccf006dc4b40e0daa27c3a63cca110b5f85540895bb17dc3dd0ca2309e225'
            '70d1b23db645e7e81ccfd9db0f6cd8c2c006bd54fe76924171dd09bd42130f5a'
            '9fe909eb60cadf8640b13cf8e75b1307ee98519857d3f9f0a3510967ce56e0b1'
            '0744ab97e8a5256e403632713df89ef0784e0429f703b625b556c983ad1d8fac'
            '1b1a8a43d6395b42dc903cd7f451d6345a45202763c9b804af8da6937e5e45c1'
            '80de3facade04b394a501f13dd1c16d66381715c42c1f597fc1142cdcbe5f3de'
            'a01db0c9af3c03fa5cb8f72ff3f2f315949e99a2a9a894e3c6c5ba340a78c979'
            '3f262ef4b3e752992667ab482cbf364e3b9e6f95b4b6fb12a1ce6fa7a88f124e'
            '1447876c2d2dcf48c8e94c3bffbb09f1b4005621a55f78fb7d9faecebdb26264'
            '90a0003840fc9310f22b26e909845d5909a515dbf3f5aff39f730b190e808597'
            '72dffe1ee4ae96a966a301dd1486832ce823cf3132f3ab1cd4ddb75ef9816d08'
            '8825623ea18abb8bfb3a8811b6c59dc8485f7d767c6f3a013fdc1b1afc979426'
            '51603633d8c81211061b7fa0f6a74ac69a0cbb7cad67eb4a7187ed5bcfefe308'
            '087ce2c9adffcd88f4a67c881215a451e35e02a6a4a155d76321e05453d80f5d')
sha256sums_x86_64=('7816e50b2f311e39553d93d016e9cb37435fbc9d9b20ffa2290c2ae8619e15e2')


_platform="$(test "${CARCH}" = 'x86_64' && echo "${_build64name}" || echo "${_build32name}")"
_pkgstring="${pkgname}-${_platform}-${_srcver}-${_binver}"

# Make a string suitable for `sed`, by escaping `[]/&$.*^\` - syntax: `_sed_escape STRING`
_sed_escape() {

    echo "${1}" | sed 's/[]\/&.*$^[]/\\&/g'

}

prepare() {
    cd "${srcdir}"

    msg 'Extracting package...'
    chmod +x "${srcdir}/bitrock-unpacker.tcl"
    rm -rf "${srcdir}/${_pkgstring}"
        "${srcdir}/bitrock-unpacker.tcl" "${srcdir}/${_pkgstring}-installer.run" \
        "${srcdir}/${_pkgstring}"
}

package() {
    depends=('inetutils' 'net-tools')
    optdepends=('polkit: for launching XAMPP Manager and Control Panel from menu'
                'pygtk: for using XAMPP Control Panel')

    # This is a constant, you should not change it - this path is hard-coded in some of the files
    local _xampp_root='/opt/lampp'

    cd "${srcdir}"

    # Package tree
    msg 'Creating package tree...'
    install -dm755 "${pkgdir}${_xampp_root}"
    rsync -azq --remove-source-files \
        "${srcdir}/${_pkgstring}/xampp_core_files/xampp_core_folder"/. \
        "${srcdir}/${_pkgstring}/xampp_developer_files/xampp_developer_folder"/. \
        "${srcdir}/${_pkgstring}/native_apache_adapter/apache_xampp_linux"/. \
        "${srcdir}/${_pkgstring}/native_proftpd_adapter/proftpd_xampp_linux"/. \
        "${srcdir}/${_pkgstring}/native_mysql_adapter/mysql_xampp_linux"/. \
        "${srcdir}/${_pkgstring}/manager/binary"/. \
        "${srcdir}/${_pkgstring}/common_native_adapter/common"/. \
        "${pkgdir}${_xampp_root}"

    rm "${pkgdir}${_xampp_root}/ctlscript.bat" "${pkgdir}${_xampp_root}/killprocess.bat"

    # Root location in all files
    msg 'Setting root location globally (it might take a few minutes)...'
    find "${pkgdir}${_xampp_root}/" -type f \
        -exec sed -i "s/@@BITNAMI_XAMPP_ROOT@@\|@@BITROCK_INSTALLDIR@@/$(_sed_escape "${_xampp_root}")/gI" '{}' \;

    # Temp folders
    install -dm777 "${pkgdir}${_xampp_root}/phpmyadmin/tmp"
    chmod 777 "${pkgdir}${_xampp_root}/temp"

    local _extension_dir="$(pcregrep -o1 -o2 -o3 '^\s*extension_dir='\''([^\'\'']+)'\''\s*$|^\s*extension_dir="([^\"]+)"\s*$|^\s*extension_dir=([^\s]+)\s*$' "${pkgdir}${_xampp_root}/bin/php-config")"

    local _sed_subst="
        s/@XAMPP_VERSION@/$(_sed_escape "${_srcver}-${_binver}")/g
        s/@XAMPP_PLATFORM@/$(_sed_escape "${_platform}")/g
        s/@XAMPP_ROOT@/$(_sed_escape "${_xampp_root}")/g
        s/@XAMPP_EXTENSIONDIR@/$(_sed_escape "${_extension_dir}")/g
    "

    # Links and missing files
    sed "${_sed_subst}" "${srcdir}/properties.ini.in" > "${pkgdir}${_xampp_root}/properties.ini"
    echo -n "${_srcver}-${_binver}" > "${pkgdir}${_xampp_root}/lib/VERSION"
    ln -s "${_xampp_root}/xampp" "${pkgdir}${_xampp_root}/lampp"
    test -d "${pkgdir}${_xampp_root}/share/lampp" || \
        ln -sf "${_xampp_root}/share/xampp" "${pkgdir}${_xampp_root}/share/lampp"

    msg 'Copying executables and launcher...'

    # Licenses
    install -dm755 "${pkgdir}/usr/share/licenses"
    chmod -R a+rX,u+w "${pkgdir}${_xampp_root}/licenses"
    ln -s "${_xampp_root}/licenses" "${pkgdir}/usr/share/licenses/xampp"

    # Executables
    install -dm755 "${pkgdir}/usr/bin"
    ln -s "${_xampp_root}/xampp" "${pkgdir}/usr/bin/xampp"
    sed "${_sed_subst}" "${srcdir}/xampp-manager.in" > "${pkgdir}/usr/bin/xampp-manager"
    sed "${_sed_subst}" "${srcdir}/xampp-control-panel.in" > "${pkgdir}/usr/bin/xampp-control-panel"
    chmod +x "${pkgdir}/usr/bin/xampp-control-panel" "${pkgdir}/usr/bin/xampp-manager"

    # Systemd files
    install -dm755 \
        "${pkgdir}/usr/lib/systemd/system" \
        "${pkgdir}/usr/lib/sysusers.d" \
        "${pkgdir}/usr/lib/tmpfiles.d"

    sed "${_sed_subst}" "${srcdir}/xampp.service.in" > "${pkgdir}/usr/lib/systemd/system/xampp.service"
    install -Dm644 "${srcdir}/xampp.sysusers" "${pkgdir}/usr/lib/sysusers.d/xampp.conf"
    sed "${_sed_subst}" "${srcdir}/xampp.tmpfiles.in" > "${pkgdir}/usr/lib/tmpfiles.d/xampp.conf"

    # Polkit files
    install -dm755 "${pkgdir}/usr/share/polkit-1/actions"
    install -Dm644 "${srcdir}/org.apachefriends.xampp.policy" \
        "${pkgdir}/usr/share/polkit-1/actions/org.apachefriends.xampp.policy"

    # Appstream
    install -Dm644 "${srcdir}/org.xampp-control-panel.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.xampp-control-panel.metainfo.xml"
    install -Dm644 "${srcdir}/org.xampp-manager.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.xampp-manager.metainfo.xml"

    # Launchers and icons
    install -dm755 \
        "${pkgdir}/usr/share/icons/hicolor/scalable/apps" \
        "${pkgdir}/usr/share/applications"

    install -Dm644 "${srcdir}/xampp-control-panel.desktop" "${pkgdir}/usr/share/applications/org.xampp_control_panel.desktop"
    install -Dm644 "${srcdir}/xampp-manager.desktop" "${pkgdir}/usr/share/applications/org.xampp_manager.desktop"
    install -Dm644 "${srcdir}/xampp.svg" "${pkgdir}/usr/share/icons/hicolor/scalable/apps/xampp.svg"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    # Integration with the Filesystem Hierarchy Standard
    ln -s "${_xampp_root}" "${pkgdir}/usr/share/xampp"
}
